/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

public class SerialInterface implements SerialPortEventListener {

	private static SerialPort serialPort;
	private static String buffer = "";
	private ItemsDB itemsDB = null;

	public SerialInterface(ItemsDB itemsDB){
		if(!this.initaliseSerialPort()){
			System.out.println("Error opening Serial Port");
			//System.exit(1);
		}
		this.itemsDB = itemsDB;
	}

	/**
	 * Initializes the serial port by selecting the first available serial port
	 * @return
	 * <tt>true</tt> if serial port is not null and is open
	 * <tt>false</tt> if post is null or port is not open
	 */
	private boolean initaliseSerialPort(){
		String[] portNames = SerialPortList.getPortNames();
		for(int i = 0; i < portNames.length; i++){
			System.out.println("Currently using "+portNames[i]);
		}
		if(portNames.length > 0){
			//portName = portNames[0];
			//serialPort = new SerialPort("COM5");
			serialPort = new SerialPort(portNames[0]);
		}
		try {
			if((serialPort!=null)&&(serialPort.openPort())){
				serialPort.setParams(9600, 8, 1, 0);
				int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
				serialPort.setEventsMask(mask);
				serialPort.addEventListener(this);
				return true;
			}else{
				return false;
			}
		} catch (SerialPortException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void serialEvent(SerialPortEvent arg0) {
		if(arg0.isRXCHAR()){//If data is available
			//System.out.println("event value:" + arg0.getEventValue());
			try {
				buffer = buffer + serialPort.readString(arg0.getEventValue());
				//System.out.println("Read Line:"+buffer);
				inputValidation();
			}catch (SerialPortException ex) {
				ex.printStackTrace();
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		else if(arg0.isCTS()){//If CTS line has changed state
			if(arg0.getEventValue() == 1){//If line is ON
				System.out.println("CTS - ON");
			}
			else {
				System.out.println("CTS - OFF");
			}
		}
		else if(arg0.isDSR()){///If DSR line has changed state
			if(arg0.getEventValue() == 1){//If line is ON
				System.out.println("DSR - ON");
			}
			else {
				System.out.println("DSR - OFF");
			}
		}

	}

	/*
	 * RFID Code length is 14 bytes/char including 0x02 starting and 0x03 end point
	 * validates the static class String buffer to the first valid input in the buffer
	 */
	private void inputValidation(){
		byte inputBytes[] = buffer.getBytes();
		int count = 0;
		if(inputBytes.length<14){
			return;
		}
		//go to start of input string
		for(count = 0; count< inputBytes.length;count++){
			if(inputBytes[count]==0x02){
				count++;
				break;
			}
		}
		//prevent array out of bounds exception
		if(count >= inputBytes.length){
			return;
		}
		int count1;
		for(count1 = count;(count1 < inputBytes.length);count1++){
			//if complete, substring to the exact string required 
			if(inputBytes[count1]==0x0d){
				buffer = buffer.substring(count, count1);
				break;
			}
		}
		if(buffer.length()==12){
			//System.out.println("\""+buffer+"\"|length:"+buffer.length());
			itemsDB.serialInput(buffer);
		}else{
			//System.out.println("\""+buffer+"\"|length:"+buffer.length());
			return;
		}
	}

	public boolean closeSerialPort(){
		if(serialPort==null){
			System.out.println("Serial Port is null!");
			return false;
		}
		if(serialPort.isOpened()){
			try {
				if(serialPort.closePort()){
					return true;
				}else {
					return false;
				}
			} catch (SerialPortException e) {
				e.printStackTrace();
				return false;
			}
		}else{
			System.out.println("Serial Port alread closed!");
			return false;
		}
	}
}
