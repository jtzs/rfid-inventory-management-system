/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class MainPanel extends JPanel{

	private static ArrayList<ArrayList<JTextField>> itemList = null;
	private static ArrayList<JLabel> header = null;
	
	public MainPanel(){
		header = new ArrayList<JLabel>();
		header.add(new JLabel("Name"));
		header.add(new JLabel("RFID Code"));
		header.add(new JLabel("Perishability"));
		header.add(new JLabel("Expiry Date"));
		header.add(new JLabel("Reusability"));
		header.add(new JLabel("Quantity"));
		header.add(new JLabel("Created Date"));
		header.add(new JLabel("On Loan"));
		itemList = new ArrayList<ArrayList<JTextField>>();
		setLayout(new GridLayout(0,8));
		printItems();
	}
	
	public static boolean addItem(Item item){
		ArrayList<JTextField> temp = new ArrayList<JTextField>();
		JTextField name = new JTextField();
		name.setText(item.getName());
		temp.add(name);
		JTextField RFIDCode = new JTextField();
		RFIDCode.setText(item.getRFIDCode());
		temp.add(RFIDCode);
		JTextField perishable = new JTextField();
		perishable.setText(item.getPerishable()+"");
		temp.add(perishable);
		JTextField expiryDate = new JTextField();
		expiryDate.setText(item.getExpiryDate());
		temp.add(expiryDate);
		JTextField reusability = new JTextField();
		reusability.setText(item.getReusablility()+"");
		temp.add(reusability);
		JTextField quantity = new JTextField();
		quantity.setText(item.getQuantity()+"");
		temp.add(quantity);
		JTextField createdDate = new JTextField();
		createdDate.setText(item.getCreatedDate());
		temp.add(createdDate);
		JTextField onLoan = new JTextField();
		onLoan.setText(item.onLoan()+"");
		temp.add(onLoan);
		itemList.add(temp);
		return true;
	}
	
	public void printItems(){
		removeAll();
		for(JLabel jlabel : header){
			add(jlabel);
		}
		for(ArrayList<JTextField> item : itemList){
			for(JTextField itemChar : item){
				itemChar.setEditable(false);
				add(itemChar);
			}
		}
		repaint();
	}
	
	public void clearItems(){
		itemList.clear();
	}
}