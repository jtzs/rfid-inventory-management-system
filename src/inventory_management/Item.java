/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

public class Item{
	private String name = "";
	private String RFIDCode = "";
	private boolean perishable = false;
	private String expiryDate = "";
	private boolean reusability = false;
	private int quantity = 0;
	private String createdDate = "";
	private boolean onLoan = false;	//always false for non-reusable items
	private String uploaded = "";

	public Item(){}
	public Item(String name,String RFIDCode, boolean perishable, String expiryDate, boolean reusability, int quantity){
		this.name = name;
		this.RFIDCode = RFIDCode;
		this.perishable = perishable;
		this.expiryDate = expiryDate;
		this.reusability = reusability;
		this.quantity = quantity;
		this.createdDate = Records.getNow();
		this.uploaded = "99999999999999"; //according to the format yyyyMMddHHmmss
	}

	private Item(String name,String RFIDCode, boolean perishable, String expiryDate, boolean reusability, int quantity,String createdDate,boolean onLoan, String uploaded){
		this.name = name;
		this.RFIDCode = RFIDCode;
		this.perishable = perishable;
		this.expiryDate = expiryDate;
		this.reusability = reusability;
		this.quantity = quantity;
		this.createdDate = createdDate;
		this.onLoan = onLoan;
		this.uploaded = uploaded;
	}

	public String toString(){
		return name + " " + RFIDCode + " " + perishable + " " + expiryDate + " " + reusability + " " + quantity + " " + createdDate + " " + onLoan +" " + uploaded;
	}

	public static Item fromString(String string){
		String name = string.split(" ")[0];
		String RFIDCode = string.split(" ")[1];
		boolean perishable;
		if(string.split(" ")[2].equalsIgnoreCase("true")){
			perishable = true;
		}else perishable = false;
		String expiryDate = string.split(" ")[3];
		boolean reusability;
		if(string.split(" ")[4].equalsIgnoreCase("true")){
			reusability = true;
		}else reusability = false;
		int quantity = Integer.parseInt(string.split(" ")[5]);
		String createdDate = string.split(" ")[6];
		boolean onLoan;
		if(string.split(" ")[7].equalsIgnoreCase("true")){
			onLoan = true;
		}else onLoan = false;
		String uploaded = string.split(" ")[8];
		return new Item(name,RFIDCode,perishable,expiryDate,reusability,quantity,createdDate,onLoan,uploaded);
	}

	public boolean equals(Object o){
		if(o.getClass().toString() == this.getClass().toString()){
			Item temp = (Item)o;
			if(temp.getRFIDCode().equalsIgnoreCase(this.getRFIDCode())){
				return true;
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean getPerishable() {
		return perishable;
	}
	public void setPerishable(boolean perishable) {
		this.perishable = perishable;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return
	 * <tt>true</tt> if item is reusable
	 * <tt>false</tt> if item is not reusable
	 */
	public boolean getReusablility() {
		return reusability;
	}
	public void setReusability(boolean reusability) {
		this.reusability = reusability;
	}
	public String getRFIDCode(){
		return this.RFIDCode;
	}
	public int getQuantity(){
		return this.quantity;
	}
	public String getCreatedDate(){
		return this.createdDate;
	}
	public String getUploaded(){
		return this.uploaded;
	}
	public void setUploaded(String uploaded){
		this.uploaded = uploaded;
	}
	
	/**
	 *@return
	 *<tt>true</tt> if item is on loan<br/>
	 *<tt>false</tt> for all non reusable items<br/>
	 */
	public boolean onLoan(){
		return onLoan;
	}

	/**
	 * @return
	 * <tt>true</tt> if reusable and not on loan 
	 * <tt>true</tt> if not reusable and quantity is > 0
	 * <tt>false</tt> otherwise
	 */
	public boolean loanItem(){
		if(reusability){
			if(onLoan){
				return false;
			}else{
				onLoan = true;
				//depreciated
				//ItemsDB.getTransactionDB().add(new Transaction(this));
				ItemsDB.addTransaction(new Transaction(this));
				return true;
			}
		}else{
			if(quantity > 0){
				quantity--;
				//depreciated
				//ItemsDB.getTransactionDB().add(new Transaction(this));
				ItemsDB.addTransaction(new Transaction(this));
				return true;
			}else {
				return false;
			}
		}
	}
	
	/**
	 * @return
	 * <tt>false</tt> for all non reusable items<br/>
	 * <tt>true</tt> if item is reusable and on loan<br/>
	 * <tt>false</tt> if items is reusable and not on loan<br/>
	 */
	//TODO
	public boolean returnItem(){
		if(reusability){
			if(onLoan){
				if(ItemsDB.getItemTransaction(RFIDCode).returnItem()){
					onLoan = false;
					quantity++;
					return true;
				}
				return false;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}