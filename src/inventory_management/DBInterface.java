/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */

package inventory_management;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBInterface {
	private Connection DBC;
	private String ip = null;
	final private String selectItemString = "SELECT `name`, `RFIDCode`, `perishable`, `expiryDate`, `reusability`, `quantity`, `createdDate`, `onLoan`, `TIMESTAMP` FROM `item` WHERE `RFIDCode`=?";
	final private String insertItemString = "INSERT INTO `item`(`name`, `RFIDCode`, `perishable`, `expiryDate`, `reusability`, `quantity`, `createdDate`, `onLoan`) VALUES (?,?,?,?,?,?,?,?)";
	final private String updateItemString = "UPDATE `item` SET `name`=?,`RFIDCode`=?,`perishable`=?,`expiryDate`=?,`reusability`=?,`quantity`=?,`createdDate`=?,`onLoan`=? WHERE `RFIDCode`=?";
	final private String selectTransactionString = "SELECT `itemRFIDCode`, `transactionID`, `borrowDate`, `toBeReturned`, `returnDate`, `TIMESTAMP` FROM `transaction` WHERE `transactionID`=?";
	final private String insertTransactionString = "INSERT INTO `transaction`(`itemRFIDCode`, `transactionID`, `borrowDate`, `toBeReturned`, `returnDate`) VALUES (?,?,?,?,?)";
	final private String updateTransactionString = "UPDATE `transaction` SET `itemRFIDCode`=?,`transactionID`=?,`borrowDate`=?,`toBeReturned`=?,`returnDate`=? WHERE `transactionID`=?";
	final private String getAllItemString = "SELECT * FROM `item`";
	final private String getAllTransactionString = "SELECT * FROM `transaction`";
	private PreparedStatement selectItemPS = null;
	private PreparedStatement insertItemPS = null;
	private PreparedStatement updateItemPS = null;
	private PreparedStatement selectTransactionPS = null;
	private PreparedStatement insertTransactionPS = null;
	private PreparedStatement updateTransactionPS = null;
	private PreparedStatement getAllItemPS = null;
	private PreparedStatement getAllTransactionPS = null;
	private PreparedStatement getTimestampPS = null;
	private ResultSet selectItemRS = null;
	private ResultSet insertItemRS = null;
	private ResultSet selectTransactionRS = null;
	private ResultSet insertTransactionRS = null;
	private ResultSet getAllItemRS = null;
	private ResultSet getAllTransactionRS = null;
	private ResultSet getTimestampRS = null; 
	private boolean serverAvailable;

	/**
	 * Constructor
	 * @param IP the ip address of the DB server containing the Item DB
	 */
	public DBInterface(String ip) {
		this.ip = ip;
		DBC = getConnection(ip);
		if(serverAvailable){
			System.out.println("Connection to DB Complete");
		}else{
			System.out.println("DB unavailable");
		}
	}

	/**
	 * attempts to reconnect to the server
	 * @return
	 * <tt>true</tt> if the server is successfully connected
	 * <tt>false</tt> if the server is unavailable
	 */
	public boolean reconnect(){
		DBC = getConnection(ip);
		if(serverAvailable){
			System.out.println("Connection to DB Complete");
		}else{
			System.out.println("DB unavailable");
		}
		return serverAvailable;
	}

	/**
	 * Open Connection
	 */
	private Connection getConnection(String ip){
		Connection conn = null;
		try {
			Properties connectionProps = new Properties();
			connectionProps.put("user", "user");
			connectionProps.put("password", "pass");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection("jdbc:mysql://"+ip+":3306/inventory_management", connectionProps);
			serverAvailable = true;
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			serverAvailable = false;
			return null;
		}catch(InstantiationException e) {
			e.printStackTrace();
			serverAvailable = false;
			return null;
		}catch(IllegalAccessException e) {
			e.printStackTrace();
			serverAvailable = false;
			return null;
		}catch(Exception e) {
			e.printStackTrace();
			serverAvailable = false;
			return null; 
		}
		return conn;
	}

	/**
	 * Closes the Connection
	 */
	public void close() {
		if(DBC!=null) {
			serverAvailable = false;
			try {
				DBC.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * downloads the server's item and transaction tables to supplement/update those loaded from the files
	 */
	public boolean initialiseDB(){
		boolean toreturn = true;
		if(serverAvailable){
			if(!getAllItems()){
				System.out.println("Error updating Items");
				toreturn = false;
			}
			if(!getAllTransactions()){
				System.out.println("Error updating Transactions");
				toreturn = false;
			}
		}else{
			return false;
		}
		return toreturn;
	}

	/**
	 * uploads the current item and transaction DB to the DB server
	 */
	public boolean uploadDB(){
		int itemUploaded=0,transactionUploaded = 0;
		boolean toreturn = true;
		String timestamp = "";
		if(serverAvailable){
			for(int count = 0; count <ItemsDB.getItemDB().size(); count++){
				//for testing
				//System.out.println(ItemsDB.getItemDB().get(count).getUploaded()+" vs. "+getTimestamp("item",ItemsDB.getItemDB().get(count).getRFIDCode()));
				//System.out.println(ItemsDB.getItemDB().get(count).getUploaded().compareTo(getTimestamp("item",ItemsDB.getItemDB().get(count).getRFIDCode())));
				if(ItemsDB.getItemDB().get(count).getUploaded().compareTo(getTimestamp("item",ItemsDB.getItemDB().get(count).getRFIDCode()))>=0){
					timestamp = sendItem(ItemsDB.getItemDB().get(count));
					if(timestamp==null){
						toreturn = false;
					}else{
						ItemsDB.getItemDB().get(count).setUploaded(timestamp);
						itemUploaded++;
					}
				}
			}
			for(int count = 0; count <ItemsDB.getTransactionDB().size(); count++){
				//for testing
				//System.out.println(ItemsDB.getTransactionDB().get(count).getUploaded()+" vs. "+getTimestamp("transaction",ItemsDB.getTransactionDB().get(count).getTransactionID()));
				//System.out.println(ItemsDB.getTransactionDB().get(count).getUploaded().compareTo(getTimestamp("transaction",ItemsDB.getTransactionDB().get(count).getTransactionID())));
				if(ItemsDB.getTransactionDB().get(count).getUploaded().compareTo(getTimestamp("transaction",ItemsDB.getTransactionDB().get(count).getTransactionID()))>=0){
					timestamp = sendTransaction(ItemsDB.getTransactionDB().get(count));
					if(timestamp==null){
						toreturn = false;
					}else{
						ItemsDB.getTransactionDB().get(count).setUploaded(timestamp);
						transactionUploaded++;
					}
				}
			}
		}else{
			return false;
		}
		System.out.println(itemUploaded+" items uploaded");
		System.out.println(transactionUploaded+" transactions uploaded");
		return toreturn;
	}

	/**
	 * sends the transaction to the server<br/>
	 * updates if the transaction ID exists<br/>
	 * inserts if transaction ID does not exist<br/>
	 * @param transaction the transaction which needs to be sent
	 * @return
	 * the timestamp of the update/insert<br/>
	 * null if not updated/inserted
	 */
	public String sendTransaction(Transaction transaction){
		if(updateTransaction(transaction)==0){
			if(insertTransaction(transaction)){
				return null;
			}
		}
		return getTimestamp("transaction",transaction.getTransactionID());
	}

	/**
	 * uploads the item to the server
	 * updates if RFID Code already exists
	 * inserts if the RFID code does not
	 * @param item the item which needs to be sent
	 * @return 
	 * the timestamp if the insert<br/>
	 * null if not updated/inserted<br/>
	 */
	public String sendItem(Item item){
		if(updateItem(item)==0){
			if(insertItem(item)){
				return null;
			}
		}
		return getTimestamp("item",item.getRFIDCode());
	}

	/**
	 * gets the item form the server
	 * searches for the item from the server
	 * @return
	 * null if RFIDCode is not found
	 */
	private Item selectItem(String RFIDCode){
		Item toreturn = null;
		try {
			if(selectItemPS == null){
				selectItemPS = DBC.prepareStatement(selectItemString);
			}
			selectItemPS.setString(1, RFIDCode);
			selectItemRS = selectItemPS.executeQuery();
			if(selectItemRS.next()){
				//workaround to use a private constructor
				toreturn = Item.fromString(
						selectItemRS.getString("name")+" "+
								selectItemRS.getString("RFIDCode") + " "+
								selectItemRS.getBoolean("perishable") + " "+
								selectItemRS.getString("expiryDate") + " "+
								selectItemRS.getBoolean("reusability") + " "+
								selectItemRS.getInt("quantity") + " "+
								selectItemRS.getString("createdDate") + " "+
								selectItemRS.getBoolean("onLoan") + " "+
								Records.formatTimestamp(selectItemRS.getTimestamp("TIMESTAMP"))
						);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(selectItemPS!=null){
				try {
					selectItemPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(selectItemRS!=null){
				try {
					selectItemRS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return toreturn;
	}

	private boolean insertItem(Item item){
		try{
			insertItemPS = DBC.prepareStatement(insertItemString);
			insertItemPS.setString(1, item.getName());
			insertItemPS.setString(2, item.getRFIDCode());
			insertItemPS.setBoolean(3, item.getPerishable());
			insertItemPS.setString(4, item.getExpiryDate());
			insertItemPS.setBoolean(5, item.getReusablility());
			insertItemPS.setInt(6, item.getQuantity());
			insertItemPS.setString(7, item.getCreatedDate());
			insertItemPS.setBoolean(8, item.onLoan());
			return insertItemPS.execute();
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			if(insertItemPS!=null){
				try {
					insertItemPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	private int updateItem(Item item){
		try {
			updateItemPS = DBC.prepareStatement(updateItemString);

			//if exist update
			updateItemPS.setString(1, item.getName());
			updateItemPS.setString(2, item.getRFIDCode());
			updateItemPS.setBoolean(3, item.getPerishable());
			updateItemPS.setString(4, item.getExpiryDate());
			updateItemPS.setBoolean(5, item.getReusablility());
			updateItemPS.setInt(6, item.getQuantity());
			updateItemPS.setString(7, item.getCreatedDate());
			updateItemPS.setBoolean(8, item.onLoan());
			//where
			updateItemPS.setString(9, item.getRFIDCode());
			return updateItemPS.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(updateItemPS!=null){
				try {
					updateItemPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}

	private Transaction selectTransaction(String transactionID){
		try {
			selectTransactionPS = DBC.prepareStatement(selectTransactionString);
			selectTransactionPS.setString(1, transactionID);
			selectTransactionRS = selectTransactionPS.executeQuery();
			return Transaction.fromString(
					selectTransactionRS.getString("itemRFIDCode") +" "+
							selectTransactionRS.getString("transactionID") +" "+
							selectTransactionRS.getString("borrowDate") +" "+
							selectTransactionRS.getString("returnDate") +" "+
							Records.formatTimestamp(selectTransactionRS.getTimestamp("TIMESTAMP"))
					);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(selectTransactionPS!=null){
				try {
					selectTransactionPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(selectTransactionRS!=null){
				try {
					selectTransactionRS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	private boolean insertTransaction(Transaction transaction){
		try {
			insertTransactionPS = DBC.prepareStatement(insertTransactionString);
			insertTransactionPS.setString(1, transaction.getItemRFIDCode());
			insertTransactionPS.setString(2, transaction.getTransactionID());
			insertTransactionPS.setString(3, transaction.getBorrowDate());
			insertTransactionPS.setBoolean(4, transaction.toBeReturned());
			insertTransactionPS.setString(5, transaction.getReturnDate());
			return insertTransactionPS.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(insertTransactionPS!=null){
				try {
					insertTransactionPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	private int updateTransaction(Transaction transaction){
		try {
			updateTransactionPS = DBC.prepareStatement(updateTransactionString);
			updateTransactionPS.setString(1, transaction.getItemRFIDCode());
			updateTransactionPS.setString(2, transaction.getTransactionID());
			updateTransactionPS.setString(3, transaction.getBorrowDate());
			updateTransactionPS.setBoolean(4, transaction.toBeReturned());
			updateTransactionPS.setString(5, transaction.getReturnDate());
			//where
			updateTransactionPS.setString(6, transaction.getTransactionID());
			return updateTransactionPS.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			if(updateTransactionPS!=null){
				try {
					updateTransactionPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}
	/**
	 * downloads all transaction from the server
	 * if the transaction ID already exists in the transaction DB and the uploaded date is not before the server timestamp it does not add the transaction
	 */
	private boolean getAllTransactions(){
		int numDownloaded = 0;
		try {
			getAllTransactionPS = DBC.prepareStatement(getAllTransactionString);
			getAllTransactionRS = getAllTransactionPS.executeQuery();
			while(getAllTransactionRS.next()){
				if(		(ItemsDB.containsTransaction(getAllTransactionRS.getString("transactionID"))==null)||
						(ItemsDB.containsTransaction(getAllTransactionRS.getString("transactionID")).getUploaded().compareTo(Records.formatTimestamp(getAllTransactionRS.getTimestamp("TIMESTAMP")))>0)){
					numDownloaded++;
					ItemsDB.addTransaction(Transaction.fromString(
							getAllTransactionRS.getString("itemRFIDCode") +" "+
									getAllTransactionRS.getString("transactionID") +" "+
									getAllTransactionRS.getString("borrowDate") +" "+
									getAllTransactionRS.getString("returnDate") +" "+
									Records.formatTimestamp(getAllTransactionRS.getTimestamp("TIMESTAMP"))
							));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(getAllTransactionPS!=null){
				try {
					getAllTransactionPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(getAllTransactionRS!=null){
				try {
					getAllTransactionRS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			System.out.println(numDownloaded+" transactions downloaded");
		}
		return true;
	}

	/**
	 * downloads all items from the server
	 * if the item's RFID Code already exists in the item DB and the uploaded date is not before the server timestamp it does not add the item
	 */
	private boolean getAllItems(){
		int numDownloaded = 0;
		try {
			getAllItemPS = DBC.prepareStatement(getAllItemString);
			getAllItemRS = getAllItemPS.executeQuery();
			while(getAllItemRS.next()){
				if(		( ItemsDB.getItemByRFIDCode(getAllItemRS.getString("RFIDCode"))==null )||
						( ItemsDB.getItemByRFIDCode(getAllItemRS.getString("RFIDCode")).getUploaded().compareTo(Records.formatTimestamp(getAllItemRS.getTimestamp("TIMESTAMP")))<0 )){
					numDownloaded++;
					ItemsDB.addItem(Item.fromString(
							getAllItemRS.getString("name") +" "+
									getAllItemRS.getString("RFIDCode") +" "+
									getAllItemRS.getBoolean("perishable") +" "+
									getAllItemRS.getString("expiryDate") +" "+
									getAllItemRS.getBoolean("reusability") +" "+
									getAllItemRS.getInt("quantity") +" "+
									getAllItemRS.getString("createdDate") +" "+
									getAllItemRS.getBoolean("onLoan") +" "+
									Records.formatTimestamp(getAllItemRS.getTimestamp("TIMESTAMP"))
							));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(getAllItemPS!=null){
				try {
					getAllItemPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(getAllItemRS!=null){
				try {
					getAllItemRS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			System.out.println(numDownloaded+" items downloaded");
		}
		return true;
	}

	/**
	 * Timestamp is of the format yyyyMMddHHmmss
	 */
	public String getTimestamp(String table,String key){
		String itemNotFoundReturnValue = "00000000000000";
		String tableKey = "";
		if(table.equalsIgnoreCase("item")){
			tableKey = "RFIDCode";
		}else if(table.equalsIgnoreCase("transaction")){
			tableKey = "transactionID";
		}else{
			return itemNotFoundReturnValue;
		}
		try {
			getTimestampPS = DBC.prepareStatement("SELECT `TIMESTAMP` FROM "+table+" WHERE `" +tableKey+"`=?");
			getTimestampPS.setString(1, key);
			getTimestampRS = getTimestampPS.executeQuery();
			if(getTimestampRS.next()){
				return Records.formatTimestamp(getTimestampRS.getTimestamp("TIMESTAMP"));
			}else{
				return itemNotFoundReturnValue;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(getTimestampPS!=null){
				try {
					getTimestampPS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(getTimestampRS!=null){
				try {
					getTimestampRS.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return itemNotFoundReturnValue;
	}

	/**
	 * @return
	 * <tt>true</tt> if the item DB server is available
	 * <tt>false</tt> if the item DB server is unavailable
	 */
	public boolean isServerAvailable() {
		return serverAvailable;
	}
}
