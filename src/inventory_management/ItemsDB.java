/*
 * Author: Joshua Tan
 * 
 * FYP Title: RFID Based Inventory Management System
 * Nanyang Technological University,School of Computer Engineering 2013/2014
 * 
 * All rights reserved by the author of the code.
 * Please contact me if you wish to use the code.
 * 
 * God Bless.
 * 
 */
/*
 * In this version, MainPanel.java and ButtonPanel.java are not in use due to time complications.
 */

package inventory_management;

import java.util.ArrayList;
import java.util.Scanner;

public class ItemsDB implements Runnable{
	static int numOfInventory;
	private static SerialInterface serialInterface = null;
	private static ArrayList<Item> itemDB = new ArrayList<Item>();
	private static ArrayList<Transaction> transactionDB = new ArrayList<Transaction>();
	public static int waitingRead;
	private Records records = null;
	public final static Object lock = new Object();
	public static Scanner sc = null;
	private String lastRead= "";
	private static DBInterface dbInterface = null;

	public ItemsDB(){}

	public void run(){
		ItemsDB.sc = new Scanner(System.in);
		serialInterface = new SerialInterface(this);
		waitingRead = 0;
		records = new Records(this);
		dbInterface.initialiseDB();
		itemDB = records.initialiseItemsDB(itemDB);
		transactionDB = records.initialiseTransactionRecords(transactionDB);
		int temp = itemDB.size();
		System.out.println(temp + " Item(s) in Item DB");
		temp = transactionDB.size();
		System.out.println(temp + " Transaction(s) in Transaction DB");
		System.out.println("Current Timestamp is:"+Records.getNow());
		(new Thread(new Menu(this))).start();
		//System.out.println("Active Threads:"+Thread.activeCount());

	}

	public static void main(String[] args){
		if(args.length>=1){
			dbInterface = new DBInterface(args[0]);
		}else{
			dbInterface = new DBInterface("127.0.0.1");
		}
		(new Thread(new ItemsDB())).start();
	}

	/**
	 * All serialInterface inputs should go through here to be redirected according to waitingRead<br/>
	 * 0 for unexpected read
	 * @param inputString the string recieved from the RFID reader
	 */
	//TODO
	public void serialInput(String inputString){
		//System.out.println(inputString);
		Item temp = getItemByRFIDCode(inputString);
		try {
			//locks the menu thread to prevent misread
			Menu.wait = true;
			if(ItemsDB.waitingRead!=0){
				lastRead = "";
			}
			switch(ItemsDB.waitingRead){
			default:
			case 0:	//unexpectedRead 
				if(temp == null){
					if(inputString.compareTo(lastRead)!=0){
						System.out.println("Item not found\nTap again to add.");
						lastRead = inputString;
					}else{
						/*
						 * Item not in DB tapped twice
						 * adding to DB
						 */
						//Menu.unexpectedInput();
						switch(addNewItem(inputString)){
						case 0:
							System.out.println("Item added successfully");
							break;
						case 1:
							System.out.println("Failed to add item successfully");
							break;
						}
					}
				}else{
					System.out.println(temp.toString());
				}
				break;
			case 1:	//scan item
				if(temp == null){
					switch(addNewItem(inputString)){
					case 0:
						System.out.println("Item added successfully");
						break;
					case 1:
						System.out.println("Failed to add item successfully");
						break;
					}
				}else{
					//if item exists and is on loan, return
					if(getItemByRFIDCode(inputString).onLoan()){
						if(getItemByRFIDCode(inputString).returnItem()){
							System.out.println("item returned successfully");
						}else{
							System.out.println("failed to return item successfully");
						}
						//if item exists and not on loan, loan
					}else if(getItemByRFIDCode(inputString).loanItem()){
						System.out.println("Item "+inputString+" loaned successfully");
					}else{
						System.out.println("Failed to loan item "+inputString+" successfully");
					}
				}
				break;
			case 4:	//view Item
				if(temp == null){
					System.out.println("No item found.");
				}else{
					System.out.println(temp.toString());
					MainPanel.addItem(temp);
				}
				break;
			case 5:	//add new Item
				switch(addNewItem(inputString)){
				case 0:
					System.out.println("Item added successfully");
					break;
				case 1:
					System.out.println("Failed to add item successfully");
					break;
				case 2:
					System.out.println("Item already exists in the DB");
					break;
				}
				break;
			case 6:	//remove item from inventory
				switch(removeItem(inputString)){
				case 0:
					System.out.println("Item removed successfully");
					break;
				case 1:
					System.out.println("Failed to remove item successfully");
					break;
				case 2:
					System.out.println("Item does not exist in the DB");
					break;
				}
				break;
			}
			ItemsDB.waitingRead = 0;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			synchronized(lock){
				Menu.wait = false;
				lock.notify();
			}
		}
	}

	/**
	 *	checks if item exists
	 *	@param RFIDCode of the item which you wish to retrieve
	 *	@return
	 *	the item if the item exists
	 *	<tt>null</tt> if otherwise
	 */
	public static Item getItemByRFIDCode(String RFIDCode) {
		for(int count = 0; count < itemDB.size(); count++){
			if(itemDB.get(count).getRFIDCode().compareTo(RFIDCode)==0){
				return itemDB.get(count);
			}
		}
		return null;
	}

	/**
	 * listens from serial port for item<br/>
	 * check if item exists in the itemDB<br/>
	 * tries to add item<br/>
	 * @param RFIDCode the RFID Code of the item that needs to be removed
	 * @return
	 * 0 if item added successfully<br/>
	 * 1 if did not read from serial port<br/>
	 * 2 if item exists in itemDB<br/>
	 */
	public static int addNewItem(String RFIDCode){
		if (RFIDCode==null){
			return 1;
		}else if(getItemByRFIDCode(RFIDCode)!=null){
			return 2;
		}
		System.out.println("RFID Code read in:"+RFIDCode);
		String temp = "";
		String name = "";
		boolean perishable;
		String expiryDate = "";
		boolean reusability;
		int quantity = 0;
		System.out.print("Please enter the name:");
		do{
			try{
				name = ItemsDB.sc.next();
			}catch(Exception e){
				name = "";
				System.out.println("Invalid Entry\nPlease enter the name.");
			}
		}while(name.length()<=0);
		System.out.println("Please enter true if perishable, else false.");
		do{
			try{
				temp = ItemsDB.sc.next();
			}catch(Exception e){
				temp = "";
				System.out.println("Invalid Entry\nPlease enter true if perishable");
			}
		}while(temp.length()<=0);
		if(temp.substring(0, 1).equalsIgnoreCase("t")){
			perishable = true;
			System.out.println("Please enter the Expiry Date.");
			do{
				try{
					temp = ItemsDB.sc.next();
				}catch(Exception e){
					temp = "";
					System.out.println("Invalid Entry\nPlease enter the Expiry Date.");
				}
			}while(temp.length()<=0);
			expiryDate = temp;
		}else {
			perishable = false;
			expiryDate = "-";
		}
		System.out.println("Please enter true if the item is reusable.");
		do{
			try{
				temp = ItemsDB.sc.next();
			}catch(Exception e){
				temp = "";
				System.out.println("Invalid Entry\nPlease enter true if reusable");
			}
		}while(temp.length()<=0);
		if(temp.substring(0, 1).equalsIgnoreCase("t")){
			reusability = true;
		}else reusability = false;
		System.out.println("Please enter the quanity of the item.");
		do{
			try{
				temp = ItemsDB.sc.next();
				quantity = Integer.parseInt(temp);
			}catch(Exception e){
				temp = "";
				System.out.println("Invalid Entry\nPlease enter true if perishable");
			}
		}while(temp.length()<=0);
		itemDB.add(new Item(name,RFIDCode,perishable,expiryDate,reusability,quantity));
		return 0;
	}

	public boolean addNewItem(Item item) {
		if(getItemByRFIDCode(item.getRFIDCode())!=null){
			return false;
		}
		return itemDB.add(item);
	}

	/**
	 * listens from serial port for item<br/>
	 * check if item exists in the itemDB<br/>
	 * tries to remove item
	 * @param RFIDCode the RFID Code of the item that needs to be removed
	 * @return
	 * 0 if item removed successfully<br/>
	 * 1 if did not read from serial port<br/>
	 * 2 if item does not exist in itemDB<br/>
	 */
	public int removeItem(String RFIDCode){
		if(RFIDCode == null){
			return 1;
		}
		for(int count = 0; count < itemDB.size(); count++){
			if(itemDB.get(count).getRFIDCode().compareTo(RFIDCode)==0){
				itemDB.remove(count);
				return 0;
			}
		}
		return 2;
	}

	public static Transaction containsTransaction(String transactionID){
		for(int count = 0; count <transactionDB.size(); count++){
			if(transactionDB.get(count).getTransactionID().compareTo(transactionID)==0){
				return transactionDB.get(count);
			}
		}
		return null;
	}

	public static ArrayList<Item> getItemDB() {
		return itemDB;
	}

	public static ArrayList<Transaction> getTransactionDB(){
		return transactionDB;
	}

	public static void setItemDB(ArrayList<Item> itemsDB) {
		ItemsDB.itemDB = itemsDB;  
	}

	public static void setTransactionDB(ArrayList<Transaction> transactionDB){
		ItemsDB.transactionDB = transactionDB;
	}

	/**
	 * @returns
	 * <tt>true</tt> if item is on loan<br/>
	 * <tt>false</tt> for all not reusable items<br/>
	 * <tt>false</tt> if item is not in the itemDB
	 */
	public static boolean itemOnLoan(Item item){
		for(int count = 0; count<itemDB.size();count++){
			if(itemDB.get(count).equals(item)){
				if(itemDB.get(count).onLoan()){
					return true;
				}else{
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * @returns
	 * <tt>true</tt> if item is available for loan<br/>
	 * <tt>false</tt> if item is not reusable and quantity == 0<br/>
	 * <tt>false</tt> if item is reusable but on loan
	 */
	public static boolean itemAvailableForLoan(Item item){
		if(!item.getReusablility()){
			if(getItemByRFIDCode(item.getRFIDCode()).getQuantity()>0){
				return true;
			}else{
				return false;
			}
		}else{
			for(int count = 0 ; count < transactionDB.size(); count++){
				if(transactionDB.get(count).equals(item)){
					if(transactionDB.get(count).toBeReturned()){
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * @returns
	 * an <tt>ArrayList</tt> of items on loan<br/>
	 * does not consider items which are consumable
	 */
	public static ArrayList<Item> viewItemsOnLoan(){
		ArrayList<Item> toreturn = new ArrayList<Item>();
		for(int count = 0; count < itemDB.size(); count++){
			if(itemDB.get(count).onLoan()){
				toreturn.add(itemDB.get(count));
			}
		}
		return toreturn;
	}

	/**
	 * @returns
	 * <tt>ArrayList</tt> of items in the inventory<br/>
	 * includes items in inventory which have a quantity more than 0
	 */
	public static ArrayList<Item> viewItemsInInventory(){
		ArrayList<Item> toreturn = new ArrayList<Item>();
		for(int count = 0; count< itemDB.size(); count++){
			if(itemDB.get(count).getReusablility()){
				if((!itemDB.get(count).onLoan())||itemDB.get(count).getQuantity()>0){
					//item reusable and not on loan
					toreturn.add(itemDB.get(count));
				}
			}else{
				if(itemDB.get(count).getQuantity()>0){
					//item not reusable and quantity more than 0
					toreturn.add(itemDB.get(count));
				}

			}
		}
		return toreturn;
	}

	public static boolean addItem(Item item){
		for(int count = 0; count < itemDB.size();count++){
			if(itemDB.get(count).getRFIDCode().compareTo(item.getRFIDCode())==0){
				itemDB.remove(count);
				return itemDB.add(item);
			}
		}
		return itemDB.add(item);
	}

	public static boolean addTransaction(Transaction transaction){
		for(int count = 0; count < transactionDB.size();count++){
			if(transactionDB.get(count).getTransactionID().compareTo(transaction.getTransactionID())==0){
				transactionDB.remove(count);
				return transactionDB.add(transaction);
			}
		}
		return transactionDB.add(transaction);
	}

	/**
	 * @returns
	 * the latest transaction with the item as specified in the RFID String<br/>
	 * the transaction attribute toBeReturned must be <tt>true</tt><br/>
	 * function returns <tt>null</tt> if no such transaction exists<br/>
	 */
	public static Transaction getItemTransaction(String RFIDCode){
		for(int count = 0 ; count < transactionDB.size() ; count++){
			if(transactionDB.get(count).getItemRFIDCode().compareTo(RFIDCode)==0){
				if(transactionDB.get(count).toBeReturned()){
					return transactionDB.get(count);
				}
			}
		}
		return null;
	}

	public boolean uploadDB(){
		return dbInterface.uploadDB();
	}

	public boolean downloadDB(){
		return dbInterface.initialiseDB();
	}

	public boolean reconnect(){
		return dbInterface.reconnect();
	}

	/**
	 * Exits the items DB closing records and interfaces
	 */
	public void exit() {
		if(!serialInterface.closeSerialPort()){
			System.out.println("Error closing Serial Port");
		}else{
			System.out.println("Serial Port closed successfully");
		}
		if(!records.closeRecords()){
			System.out.println("Error closing records");
		}else{
			System.out.println("Records closed successfully");
		}
		if(ItemsDB.sc!=null){
			ItemsDB.sc.close();
		}
		System.exit(0);
	}

}